import java.util.Arrays;
import java.util.List;

/**
 * Created by Marcin on 2017-08-01.
 */
public class MapExample {

    public static void main(String[] args) {

        List<String> names= Arrays.asList("Peter", "Sam", "Greg", "Ryan");

        for (String name : names) {

            if(!name.equals("Same")){
                System.out.println(name);
            }
        }
        names.stream()
                .filter(MapExample::isNotSam)
                .forEach(System.out::println);


        }

    private static boolean isNotSam(String name) {
        return !name.equals("Sam");
    }
}
class User{

    private String name;

}


