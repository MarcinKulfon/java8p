import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created by Marcin on 2017-08-01.
 */
public class FilterExample {

    public static void main(String[] args) {

        List<String> names= Arrays.asList("Peter", "Sam", "Greg", "Ryan");

        for (String name : names) {

            if(!name.equals("Same")){
                System.out.println(name);
            }
        }
        names.stream()
                .filter(FilterExample::isNotSam)
                .forEach(System.out::println);


        }

    private static boolean isNotSam(String name) {
        return !name.equals("Sam");
    }
}


